package com.maxlogic.design_patterns.behavioral.strategy;

public interface ILoadBalancerStrategy {
  String assignServer();
}
